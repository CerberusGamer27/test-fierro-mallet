import UnderlineLink from "@modules/common/components/underline-link"

const EmptyCartMessage = () => {
  return (
    <div className="bg-amber-100 px-8 py-24 flex flex-col justify-center items-center text-center">
      <h1 className="text-2xl-semi">Tu carrito esta vacío</h1>
      <p className="text-base-regular mt-4 mb-6 max-w-[32rem]">
        No tienes nada en tu bolsa. Vamos a cambiar eso, utilice el enlace de abajo para empezar a navegar por nuestros productos.
      </p>
      <div>
        <UnderlineLink href="/store">Explorar Productos</UnderlineLink>
      </div>
    </div>
  )
}

export default EmptyCartMessage
