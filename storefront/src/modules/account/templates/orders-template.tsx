import OrderOverview from "../components/order-overview"

const OrdersTemplate = () => {
  return (
    <div className="w-full">
      <div className="mb-8 flex flex-col gap-y-4">
        <h1 className="text-2xl-semi">Ordenes</h1>
        <p className="text-base-regular">
          Visualiza tus ordenes previas y su estado. También puedes
          retornar o hacer cambios a tus ordenes si lo requieres.
        </p>
      </div>
      <div>
        <OrderOverview />
      </div>
    </div>
  )
}

export default OrdersTemplate
